//
//  ViewController.m
//  scrollView2-deniz
//
//  Created by User on 1/21/16.
//  Copyright (c) 2016 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height)];
    
    scrollView.backgroundColor = [UIColor grayColor];
    
    [self.view addSubview:scrollView];
    
    
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width*3, self.view.frame.size.height)];
    
    
    
    UIView* first = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 150, 150)];
    
    first.backgroundColor = [UIColor whiteColor];
    
    [scrollView addSubview:first];
    
    
    
    UIView* second = [[UIView alloc]initWithFrame:CGRectMake(150, 0, 150, 150)];
    
    second.backgroundColor = [UIColor redColor];
    
    [scrollView addSubview:second];
    
    
    
    UIView* third = [[UIView alloc]initWithFrame:CGRectMake(300, 0, 150, 150)];
    
    third.backgroundColor = [UIColor yellowColor];
    
    [scrollView addSubview:third];
    
    
    
    
    
    UIView* fourth = [[UIView alloc]initWithFrame:CGRectMake(0, 150, 150, 150)];
    
    fourth.backgroundColor = [UIColor blackColor];
    
    [scrollView addSubview:fourth];
    
    
    
    UIView* fifth = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 150, 150)];
    
    fifth.backgroundColor = [UIColor greenColor];
    
    [scrollView addSubview:fifth];
    
    
    
    UIView* sixth = [[UIView alloc]initWithFrame:CGRectMake(300, 300, 150, 150)];
    
    sixth.backgroundColor = [UIColor brownColor];
    
    [scrollView addSubview:sixth];
    
    
    
    UIView* seventh = [[UIView alloc]initWithFrame:CGRectMake(300, 150, 150, 150)];
    
    seventh.backgroundColor = [UIColor purpleColor];
    
    [scrollView addSubview:seventh];
    
    
    
    UIView* eighth = [[UIView alloc]initWithFrame:CGRectMake(150, 150, 150, 150)];
    
    eighth.backgroundColor = [UIColor cyanColor];
    
    [scrollView addSubview:eighth];
    
    
    
    UIView* nineth = [[UIView alloc]initWithFrame:CGRectMake(150, 300, 150, 150)];
    
    nineth.backgroundColor = [UIColor orangeColor];
    
    [scrollView addSubview:nineth];
    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
